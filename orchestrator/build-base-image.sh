#!/bin/bash
docker build -t orchestrator-base -f Dockerfile-base . &&
docker image tag orchestrator-base $DOCKER_HUB_ACCOUNT/orchestrator-base &&
docker image push $DOCKER_HUB_ACCOUNT/orchestrator-base
