General
1) Before the first run of the orchestrator we need to build and deploy the orchestrator-base image: ./build-base-image.sh
2) Service name must be unique and no other service should have a name containing the name of another service.
For example mariadb and mariadb2 is invalid. Use mariadb1 and mariadb2 instead. This is coming from a problem with
docker as searching for a specific service works only by filtering the services, and a filter mariadb will match both services
mariadb and mariadb1. Here is the bug ticket for this issue:
https://github.com/moby/moby/issues/32985

Local development

Prerequisites:
1) Docker installed and swarm mode set
2) Auditd installed

1) Setup environment variable: export MORCH_ENV=local
2) Create a folder that belongs to the current user for logging:
sudo mkdir /var/log/orchestrator && sudo chown stavros:stavros /var/log/orchestrator
3) Export your dockerhub account: export DOCKER_HUB_ACCOUNT='your docker hub account'
