package main

import (
	"encoding/xml"
	"fmt"
	"io/ioutil"
	"os"
)

type ADTree struct {
	XMLName xml.Name `xml:"adtree"`
	Nodes   []Node   `xml:"node"`
}

type Node struct {
	XMLName 	xml.Name `xml:"node"`
	Refinement  string   `xml:"refinement,attr"`
	Label    	string   `xml:"label"`
	Nodes  		[]Node    `xml:"node"`
}

func main() {
	xmlFile, err := os.Open("attack_tree.xml")
	if err != nil {
		panic(err)
	}

	fmt.Println("Successfully Opened attack_tree.xml")
	defer xmlFile.Close()

	byteValue, _ := ioutil.ReadAll(xmlFile)

	var attackTree ADTree
	xml.Unmarshal(byteValue, &attackTree)
	print(attackTree.Nodes)

}

func print(nodes []Node)  {
	for _, n := range nodes {
		fmt.Println("-" + n.Refinement)
		fmt.Print(n.Label)
		print(n.Nodes)
	}
}