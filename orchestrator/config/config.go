package config

import (
	"bitbucket.org/stavrosicsd/morch/common/application"
	"fmt"
	"github.com/go-yaml/yaml"
)

type Config struct {
	Application 		`yaml:"application"`
	Server				`yaml:"server"`
	Gin  				`yaml:"gin"`
	Docker  			`yaml:"docker"`
	ConfigurationEngine	`yaml:"configuration_engine"`
}

type Application struct {
	Name	string	`yaml:"name"`
}

type Server struct {
	Cert	string	`yaml:"cert"`
	Key		string	`yaml:"key"`
	Port	int		`yaml:"port"`
}

type Gin struct {
	Mode string `yaml:"mode"`
}

type Docker struct {
	Client	Client `yaml:"client"`
}

type Client struct {
	Version	string `yaml:"version"`
}

type ConfigurationEngine struct {
	Host     string `yaml:"host"`
	Port     int 	`yaml:"port"`
}

var Conf = getConf()

func getConf() Config {
	var conf Config
	err := yaml.Unmarshal(application.ApplicationConfiguration("orchestrator"), &conf); if err != nil {
		panic(fmt.Errorf("Configuration could not be loaded! Error: %v", err))
	}
	return conf
}