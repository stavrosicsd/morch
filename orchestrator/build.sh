#!/bin/bash
go build -o target/orchestrator orchestrator.go &&
docker build --build-arg DOCKER_HUB_ACCOUNT=$DOCKER_HUB_ACCOUNT -t orchestrator . &&
docker image tag orchestrator $DOCKER_HUB_ACCOUNT/orchestrator &&
docker image push $DOCKER_HUB_ACCOUNT/orchestrator &&
rm -rf target