package router

import (
	"bitbucket.org/stavrosicsd/morch/orchestrator/config"
	"bitbucket.org/stavrosicsd/morch/orchestrator/orchestrator"
	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
)

func Router() *gin.Engine {
	gin.SetMode(config.Conf.Gin.Mode)
	router := gin.Default()
	router.Use(cors.Default())
	orchestrator.OrchestratorController(router)
	return router
}
