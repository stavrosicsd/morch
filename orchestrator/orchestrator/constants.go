package orchestrator

import "time"

var ORCHESTRATION_START time.Time
var ORCHESTRATION_REQUEST_END time.Time
var ORCHESTRATION_APPLIED time.Time
var ORCHESTRATION_END time.Time