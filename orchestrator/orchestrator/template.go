package orchestrator

var entrypointTemplate =
`#!/bin/sh
{{- with .scripts }}
	{{- range . }}
{{.}} &
	{{- end }} 
{{- end }}
{{ .originalEntrypoint}} {{ .originalCommand}}`