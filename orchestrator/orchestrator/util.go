package orchestrator

import (
	"bitbucket.org/stavrosicsd/morch/common/monitoring"
	"github.com/docker/docker/api/types/swarm"
)

func extractServiceNames(swarmServices []swarm.Service) []string {
	services := make([]string, len(swarmServices))
	for i, swarmService := range swarmServices {
		services[i] = swarmService.Spec.Name
	}
	return services
}

func groupRules(serviceRuleList []monitoring.TaggedRule) (map[monitoring.Service]map[monitoring.Tool][]monitoring.Rule,
	map[monitoring.Tool]map[monitoring.Service][]monitoring.Rule) {
	rulesByServiceAndTool := make(map[monitoring.Service]map[monitoring.Tool][]monitoring.Rule)
	rulesByToolAndService := make(map[monitoring.Tool]map[monitoring.Service][]monitoring.Rule)
	for _, serviceRule := range serviceRuleList {
		tool := serviceRule.Tool
		service := serviceRule.Service
		rulesByTool, exists := rulesByServiceAndTool[service]; if !exists {
			rulesByTool = make(map[monitoring.Tool][]monitoring.Rule)
			rulesByServiceAndTool[service] = rulesByTool
		}
		rulesByServiceAndTool[service][tool] = append(rulesByServiceAndTool[service][tool],
			monitoring.Rule{serviceRule.Asset, serviceRule.Tags, serviceRule.Parameters})
		rulesByService, exists := rulesByToolAndService[tool]; if !exists {
			rulesByService = make(map[monitoring.Service][]monitoring.Rule)
			rulesByToolAndService[tool] = rulesByService
		}
		rulesByToolAndService[tool][service] = append(rulesByToolAndService[tool][service],
			monitoring.Rule{serviceRule.Asset, serviceRule.Tags, serviceRule.Parameters})
	}
	return rulesByServiceAndTool, rulesByToolAndService
}