package orchestrator

import (
	"github.com/docker/docker/api/types/swarm"
)

type swarmSecretWithTarget struct {
	secret swarm.Secret
	target string
}

type entrypoint struct {
	scripts 			[]string
	originalEntrypoint 	string
	originalCommand 	string
}