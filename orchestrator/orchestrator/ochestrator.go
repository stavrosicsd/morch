package orchestrator

import (
	"bitbucket.org/stavrosicsd/morch/common"
	"bitbucket.org/stavrosicsd/morch/common/monitoring"
	"bitbucket.org/stavrosicsd/morch/common/morch_http"
	"bitbucket.org/stavrosicsd/morch/orchestrator/config"
	"context"
	"encoding/json"
	"fmt"
	"github.com/docker/docker/api/types"
	"github.com/docker/docker/api/types/filters"
	"github.com/docker/docker/api/types/mount"
	"github.com/docker/docker/api/types/swarm"
	"github.com/docker/docker/client"
	"github.com/pkg/errors"
	log "github.com/sirupsen/logrus"
	"os"
	"strconv"
	"strings"
	"time"
)

const MORCH_ENTRYPOINT_TARGET string = "/morch-entrypoint.sh"
const MORCH_ENTRYPOINT string = "morch-entrypoint"

var dockerClient *client.Client
var configurationEngine = config.Conf.ConfigurationEngine

func InitializeDockerClient() {
	cli, err := client.NewClientWithOpts(client.WithVersion(config.Conf.Docker.Client.Version)); if err != nil {
		panic(fmt.Errorf("Failed to initialize Docker client! Error: %v",  err))
	}
	dockerClient = cli
}

func getAvailableServices() ([]string, error) {
	swarmServices, err := findAllServices(); if err != nil {
		return nil, err
	}
	return extractServiceNames(swarmServices), nil
}

func getAvailableServicesForTool(tool monitoring.Tool) ([]string, error) {
	swarmServices, err := findAllServicesByLabel(tool.String()); if err != nil {
		return nil, err
	}
	return extractServiceNames(swarmServices), nil
}

func configure(taggedRules []monitoring.TaggedRule) []error {
	rulesByServiceAndTool, rulesByToolAndService := groupRules(taggedRules)
	channel := make(chan []error)
	go configureMonitoring(rulesByServiceAndTool, channel)
	go configureLogging(rulesByToolAndService, channel)
	return collectErrors(channel)
}

func collectErrors(channel chan []error) []error{
	var errors []error
	for i := 0; i < 2; i++ {
		err := <- channel; if err != nil {
			errors = append(errors, err...)
		}
	}
	return errors
}

func configureMonitoring(rulesByServiceAndTool map[monitoring.Service]map[monitoring.Tool][]monitoring.Rule, configureChannel chan []error) {
	channel := make(chan error)
	for service, rulesByTool := range rulesByServiceAndTool {
		go configureMonitoringForService(service, rulesByTool, channel)
	}
	var errors []error
	for i := 0; i < len(rulesByServiceAndTool); i++ {
		err := <-channel; if err != nil {
			errors = append(errors, err)
		}
	}
	configureChannel <- errors
}

func configureLogging(rulesByToolAndService map[monitoring.Tool]map[monitoring.Service][]monitoring.Rule, configureChannel chan []error) {
	var serviceRulesGroupedByTool []monitoring.ServiceRulesGroupedByTool
	for tool, rulesByService := range rulesByToolAndService {
		var serviceRules []monitoring.ServiceRule
		for service, rules := range rulesByService {
			serviceRules = append(serviceRules, monitoring.ServiceRule{service, rules})
		}
		serviceRulesGroupedByTool = append(serviceRulesGroupedByTool, monitoring.ServiceRulesGroupedByTool{tool, serviceRules})
	}
	swarmService, err := findOneServiceByName("morch_filebeat"); if err != nil {
		configureChannel <- []error{err}
		return
	}
	logConfiguration, err := requestConfigurationFromEngine(serviceRulesGroupedByTool, "/logging/configuration"); if err != nil {
		configureChannel <- []error{common.MorchError {
			common.InternalServerError,
			err.Error(),
			map[string]string{"serviceRulesGroupedByTool": fmt.Sprint(serviceRulesGroupedByTool)}}}
		return
	}
	configMap := make(map[string]monitoring.ConfigurationEntry)
	for i, logConf := range logConfiguration.ConfigurationList {
		configMap["filebeat-config-" + strconv.Itoa(i)] = logConf
	}
	configureChannel <- []error{
		applyConfiguration(swarmService, configMap, logConfiguration.LogFileMounts),
	}
}

func configureMonitoringForService(service monitoring.Service, rulesByTool map[monitoring.Tool][]monitoring.Rule, channel chan error) {
	swarmService, err := findOneServiceByName(service); if err != nil {
		channel <- err
		return
	}
	configMap, logFileMounts, err := getMonitoringConfigurationForService(swarmService, rulesByTool); if err != nil {
		channel <- err
		return
	}
	channel <- applyConfiguration(swarmService, configMap, logFileMounts)
}

func getMonitoringConfigurationForService(swarmService swarm.Service,
	rulesByTool map[monitoring.Tool][]monitoring.Rule) (map[string]monitoring.ConfigurationEntry, []string, error) {
	configMap := make(map[string]monitoring.ConfigurationEntry)
	var scriptsFilepaths []string
	var logFileMounts []string
	// TODO: Run concurrently and check service to tool compatibility using labels
	serviceName := swarmService.Spec.Name
	for tool, rules := range rulesByTool {
		monitoringConfiguration, err := requestConfigurationFromEngine(rules, "/monitoring/configuration/" + string(tool)); if err != nil {
			return nil, []string{}, common.MorchError {
				common.InternalServerError,
				err.Error(),
				map[string]string{"service": serviceName, "tool": tool.String(), "rules": fmt.Sprint(rules)}}
		}
		logFileMounts = append(logFileMounts, monitoringConfiguration.LogFileMounts...)
		for i, monConfEntry := range monitoringConfiguration.ConfigurationList {
			if monConfEntry.Type == monitoring.SCRIPT {
				scriptsFilepaths = append(scriptsFilepaths, monConfEntry.Target)
			}
			configMap[serviceName+ "-" + tool.String() + strconv.Itoa(i)] = monConfEntry
		}
	}
	if len(scriptsFilepaths) > 0 {
		entrypoint, err := getEntrypoint(swarmService, scriptsFilepaths); if err != nil {
			return nil, []string{}, common.MorchError {
				common.InternalServerError,
				err.Error(),
				map[string]string{"service": serviceName, "scriptsFilepaths": fmt.Sprint(scriptsFilepaths)},
			}
		}
		configMap[serviceName + "-" + MORCH_ENTRYPOINT] = entrypoint
	}
	return configMap, logFileMounts, nil
}

func requestConfigurationFromEngine(rules interface{}, resource string) (monitoring.Configuration, error) {
	marshalledRules, err := json.Marshal(rules); if err != nil {
		return monitoring.Configuration{}, errors.New("Failed to marshal the rules!")
	}
	body, err := morch_http.DoGet(configurationEngine.Host, configurationEngine.Port, resource, marshalledRules)
	if err != nil {
		return monitoring.Configuration{}, errors.New("Failed to fetch the configuration from the engine!")
	}
	var configuration monitoring.Configuration
	err = json.Unmarshal(body, &configuration); if err != nil {
		return monitoring.Configuration{}, errors.New("Failed to unmarshal the configuration!")
	}
	return configuration, nil
}

func getEntrypoint(swarmService swarm.Service, scriptsFilepaths []string) (monitoring.ConfigurationEntry, error){
	originalEntrypoint := swarmService.Spec.TaskTemplate.ContainerSpec.Command
	if len(originalEntrypoint) == 0 {
		return monitoring.ConfigurationEntry{}, errors.New("Original entrypoint is missing!")
	}
	originalEntrypointString := strings.Join(originalEntrypoint, " ")
	originalCommand := ""
	if len(swarmService.Spec.TaskTemplate.ContainerSpec.Args) > 0 {
		originalCommand = strings.Join(swarmService.Spec.TaskTemplate.ContainerSpec.Args, " ")
	}
	entrypointValues := entrypoint{scriptsFilepaths, originalEntrypointString, originalCommand}
	entrypointContent, err := monitoring.GetTemplatedResult(entrypointTemplate, entrypointValues); if err != nil {
		log.WithFields(log.Fields{"service": swarmService.Spec.Name, "entrypointValues": entrypointValues}).Error(err)
		return monitoring.ConfigurationEntry{}, err
	}
	return monitoring.ConfigurationEntry{monitoring.SCRIPT, entrypointContent, MORCH_ENTRYPOINT_TARGET}, nil
}

func applyConfiguration(swarmService swarm.Service, configMap map[string]monitoring.ConfigurationEntry, logFileMounts []string) error {
	err := deleteOldSecretsFromService(swarmService, configMap); if err != nil {
		return err
	}
	serviceName := swarmService.Spec.Name
	context := map[string]string{"service": serviceName, "config": fmt.Sprint(configMap)}
	err = createSecrets(configMap); if err != nil {
		return common.MorchError{common.InternalServerError, err.Error(), context}
	}
	secrets, err := getSecrets(configMap); if err != nil {
		return common.MorchError{common.InternalServerError, err.Error(), context}
	}
	_, interceptEntrypointExists := configMap[serviceName + "-" + MORCH_ENTRYPOINT]
	return updateService(swarmService, secrets, logFileMounts, interceptEntrypointExists);
}

func deleteOldSecretsFromService(service swarm.Service, configMap map[string]monitoring.ConfigurationEntry) error {
	context := map[string]string{"service": service.Spec.Name, "config": fmt.Sprint(configMap)}
	secrets, err := getSecrets(configMap); if err != nil {
		return common.MorchError{common.InternalServerError, err.Error(), context}
	}
	context["secrets"] = fmt.Sprint(secrets)
	err = deleteSecretsFromService(service, secrets); if err != nil {
		return common.MorchError{common.InternalServerError, err.Error(), context}
	}
	err = deleteSecrets(secrets); if err != nil {
		return common.MorchError{common.InternalServerError, err.Error(), context}
	}
	return nil
}

func getSecrets(configMap map[string]monitoring.ConfigurationEntry) ([]swarmSecretWithTarget, error) {
	var secrets []swarmSecretWithTarget
	channel := make(chan struct {swarm.Secret; error})
	for configName := range configMap {
		go getSecret(configName, channel)
	}
	for i := 0;  i < len(configMap); i++ {
		secretResult := <-channel; if secretResult.error != nil {
			return []swarmSecretWithTarget{}, secretResult.error
		} else {
			secret := secretResult.Secret
			if secret.ID != "" {
				secrets = append(secrets, swarmSecretWithTarget{secret, configMap[secret.Spec.Name].Target})
			}
		}
	}
	return secrets, nil
}

func getSecret(secretName string, channel chan struct {swarm.Secret; error}) {
	secretListOptions := types.SecretListOptions{Filters: filters.NewArgs(filters.Arg("name", secretName))}
	secrets, err := dockerClient.SecretList(context.Background(), secretListOptions); if err != nil {
		channel <- struct {swarm.Secret; error} {swarm.Secret{}, errors.New("Docker client failed to list the secrets!")}
		return
	}
	// Docker filtering does not do exact match, thus we want to be sure that we only delete the exact secret
	for _, secret := range secrets {
		if secret.Spec.Name == secretName {
			channel <- struct {swarm.Secret; error} {secret, nil}
			return
		}
	}
	channel <- struct {swarm.Secret; error} {swarm.Secret{}, nil}
}

func deleteSecretsFromService(service swarm.Service, secrets []swarmSecretWithTarget) error {
	serviceSecrets := service.Spec.TaskTemplate.ContainerSpec.Secrets
	atLeastOneSecretExistsOnService := false
	for _, secret := range secrets {
		secretName := secret.secret.Spec.Name
		for i := 0; i < len(serviceSecrets); i++ {
			if serviceSecrets[i].SecretName == secretName {
				service.Spec.TaskTemplate.ContainerSpec.Secrets = append(service.Spec.TaskTemplate.ContainerSpec.Secrets[:i],
					service.Spec.TaskTemplate.ContainerSpec.Secrets[i+1:]...)
				atLeastOneSecretExistsOnService = true
				break
			}
		}
	}
	if atLeastOneSecretExistsOnService {
		_, err := dockerClient.ServiceUpdate(context.Background(), service.ID, service.Meta.Version, service.Spec, types.ServiceUpdateOptions{})
		if err != nil {
			return errors.New("Secrets could not be deleted from service!")
		}
	}
	return nil
}

func createSecrets(configMap map[string]monitoring.ConfigurationEntry) error {
	channel := make(chan error)
	for configName, configuration := range configMap {
		go createNewSecret(configName, configuration.Content, channel)
	}
	for i := 0; i < len(configMap); i++ {
		err := <-channel; if err != nil {
			return err
		}
	}
	return nil
}

func createNewSecret(secretName string, secretContent string, channel chan error) {
	//fmt.Printf("Number of bytes: %d\n", len([]byte(secretContent)))
	secretSpec := swarm.SecretSpec{Annotations: swarm.Annotations{Name: secretName}, Data: []byte(secretContent)}
	_, err := dockerClient.SecretCreate(context.Background(), secretSpec); if err != nil {
		channel <- fmt.Errorf("Secret could not be created! Secret: %v Error: %v", secretName, err)
		return
	}
	channel <- nil
}

func deleteSecrets(secrets []swarmSecretWithTarget) error {
	channel := make(chan error)
	for _, secret := range secrets {
		go deleteSecret(secret.secret.ID, channel)
	}
	for i := 0; i < len(secrets); i++ {
		err := <-channel; if err != nil {
			return err
		}
	}
	return nil
}

func deleteSecret(secretId string, channel chan error) {
	err := dockerClient.SecretRemove(context.Background(), secretId); if err != nil {
		channel <- fmt.Errorf("Secret %v could not be deleted! Error: %v", secretId, err)
		return
	}
	channel <- nil
}

func updateService(service swarm.Service, secrets []swarmSecretWithTarget, mounts []string, interceptEntrypointExists bool) error {
	addSecretsToService(secrets, &service)
	addMountsToService(mounts, &service)
	if interceptEntrypointExists {
		service.Spec.TaskTemplate.ContainerSpec.Command = []string{MORCH_ENTRYPOINT_TARGET}
	}
	return updateSwarmService(service)
}

func addMountsToService(mounts []string, service *swarm.Service) {
	for _, volumePath := range mounts {
		//TODO bind mounts or volumes?
		mount := mount.Mount{Type: mount.TypeBind, Source: volumePath, Target: volumePath, ReadOnly: false}
		for i, mount := range service.Spec.TaskTemplate.ContainerSpec.Mounts {
			if mount.Source == volumePath {
				service.Spec.TaskTemplate.ContainerSpec.Mounts = append(service.Spec.TaskTemplate.ContainerSpec.Mounts[:i],
					service.Spec.TaskTemplate.ContainerSpec.Mounts[i+1:]...)
				break
			}
		}
		service.Spec.TaskTemplate.ContainerSpec.Mounts = append(service.Spec.TaskTemplate.ContainerSpec.Mounts, mount)
	}
}

func addSecretsToService(secrets []swarmSecretWithTarget, service *swarm.Service) {
	for _, secret := range secrets {
		secretName := secret.secret.Spec.Name
		// TODO: Fix mods
		service.Spec.TaskTemplate.ContainerSpec.Secrets = append(service.Spec.TaskTemplate.ContainerSpec.Secrets, &swarm.SecretReference{
			File: &swarm.SecretReferenceFileTarget{
				Name: secret.target,
				UID:  "0",
				GID:  "0",
				Mode: os.FileMode(int(0555)),
			},
			SecretID:   secret.secret.ID,
			SecretName: secretName,
		})
	}
}

func updateSwarmService(service swarm.Service) error {
	_, err := dockerClient.ServiceUpdate(context.Background(), service.ID, service.Version, service.Spec, types.ServiceUpdateOptions{})

	ORCHESTRATION_REQUEST_END = time.Now()
	fmt.Printf("ORCHESTRATION REQUEST END %d, service %s\n", ORCHESTRATION_REQUEST_END.UnixNano() / 1000000, service.Spec.Name)

	for err != nil && err.Error() == "Error response from daemon: rpc error: code = Unknown desc = update out of sequence" {
		serviceInspect, _, _ := dockerClient.ServiceInspectWithRaw(context.Background(), service.ID, types.ServiceInspectOptions{})
		_, err = dockerClient.ServiceUpdate(context.Background(), serviceInspect.ID, serviceInspect.Version, service.Spec, types.ServiceUpdateOptions{})
	}

	time.Sleep(5 * time.Second)
	serviceInspect, _, _ := dockerClient.ServiceInspectWithRaw(context.Background(), service.ID, types.ServiceInspectOptions{})
	var status = serviceInspect.UpdateStatus
	for &status == nil || status.State !=  "completed" {
		serviceInspect, _, _ := dockerClient.ServiceInspectWithRaw(context.Background(), service.ID, types.ServiceInspectOptions{})
		status = serviceInspect.UpdateStatus
	}
	ORCHESTRATION_APPLIED := status.CompletedAt
	fmt.Printf("ORCHESTRATION APPLIED %d, service %s\n", ORCHESTRATION_APPLIED.UnixNano() / 1000000, service.Spec.Name)

	if err != nil {
		return common.MorchError{
			common.InternalServerError,
			"Docker client failed to update the service!",
			map[string]string{"service": fmt.Sprint(service), "error": err.Error()}}
	}
	return nil
}