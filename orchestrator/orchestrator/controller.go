package orchestrator

import (
	"bitbucket.org/stavrosicsd/morch/common/monitoring"
	"bitbucket.org/stavrosicsd/morch/common/morch_http"
	"fmt"
	"github.com/gin-gonic/gin"
	"net/http"
	"time"
)

func OrchestratorController(router *gin.Engine) {
	router.GET("/services", func(c *gin.Context) {
		services, err := getAvailableServices()
		morch_http.HandleResponse(c, services, []error{err})
	})

	router.GET("/tools/:name/services", func(c *gin.Context) {
		services, err := getAvailableServicesForTool(monitoring.Tool(c.Param("name")))
		morch_http.HandleResponse(c, services, []error{err})
	})

	router.POST("/configure", func(c *gin.Context) {
		ORCHESTRATION_START = time.Now()
		fmt.Printf("ORCHESTRATION_START: %d\n", ORCHESTRATION_START.UnixNano() / 1000000)
		var rulesData []monitoring.TaggedRule
		err := c.BindJSON(&rulesData); if err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
			return
		}
		morch_http.HandleResponse(c, nil, configure(rulesData))
		ORCHESTRATION_END = time.Now()
		fmt.Printf("ORCHESTRATION END: %d\n", ORCHESTRATION_END.UnixNano() / 1000000)
	})
}