package orchestrator

import (
	"bitbucket.org/stavrosicsd/morch/common"
	"bitbucket.org/stavrosicsd/morch/common/monitoring"
	"context"
	"fmt"
	"github.com/docker/docker/api/types"
	"github.com/docker/docker/api/types/filters"
	"github.com/docker/docker/api/types/swarm"
	"sort"
)

func findAllServices() ([]swarm.Service, error) {
	return findServices(types.ServiceListOptions{})
}

func findOneServiceByName(service monitoring.Service) (swarm.Service, error) {
	return findOneServiceBy("name", service.String())
}

func findAllServicesByLabel(label string) ([]swarm.Service, error) {
	return findAllServicesBy("label", "morch-" + label);
}

func findAllServicesBy(key string, value string) ([]swarm.Service, error) {
	return findServices(types.ServiceListOptions{Filters: filters.NewArgs(filters.Arg(key, value))})
}

func findOneServiceBy(key string, value string) (swarm.Service, error) {
	services, err := findAllServicesBy(key, value); if err != nil {
		return swarm.Service{}, err
	}
	if len(services) == 0 {
		return swarm.Service{}, common.MorchError{
			common.BadRequest, "Service not found!", map[string]string{"key": key, "value": value},
		}
	}
	if len(services) > 1 {
		return swarm.Service{}, common.MorchError{
			common.BadRequest, "More than one services found!", map[string]string{"key": key, "value": value},
		}
	}
	return services[0], nil
}

func findServices(serverListOptions types.ServiceListOptions) ([]swarm.Service, error) {
	services, err := dockerClient.ServiceList(context.Background(), serverListOptions); if err != nil {
		return []swarm.Service{}, common.MorchError{
			common.InternalServerError, "Docker client failed to list the services!",
			map[string]string{
				"error": err.Error(),
				"serverListOptions": fmt.Sprint(serverListOptions.Filters),
			},
		}
	}
	sort.Slice(services, func(i, j int) bool {
		return services[i].Spec.Name < services[j].Spec.Name
	})
	return services, err
}