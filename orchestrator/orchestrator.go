package main

import (
	"bitbucket.org/stavrosicsd/morch/common/logger"
	"bitbucket.org/stavrosicsd/morch/orchestrator/config"
	"bitbucket.org/stavrosicsd/morch/orchestrator/orchestrator"
	"bitbucket.org/stavrosicsd/morch/orchestrator/router"
	"fmt"
	"net/http"
	"strconv"
)

func main() {
	defer logger.Init(config.Conf.Application.Name).Close()
	orchestrator.InitializeDockerClient()
	server := config.Conf.Server
	err := http.ListenAndServeTLS(":" + strconv.Itoa(server.Port), server.Cert, server.Key, router.Router()); if err != nil {
		panic(fmt.Errorf("Failed to initialize the Orchestrator! Error: %v", err))
	}
}
