package common

import "strings"

func SpliceContains(s []string, e string) bool {
	for _, a := range s {
		if a == e {
			return true
		}
	}
	return false
}

func SpliceNotContains(s []string, e string) bool {
	return !SpliceContains(s, e)
}

func StringContains(e string, s []string) bool {
	for _, a := range s {
		if strings.Contains(e, a) {
			return true
		}
	}
	return false
}