package application

import (
	"fmt"
	"io/ioutil"
	"os"
)

type Environment int

const (
	local Environment = 1 + iota
	prod
)

var environments = [...]string {
	"local",
	"prod",
}

func (environment Environment) String() string {
	return environments[environment - 1]
}

func ApplicationConfiguration(application string) []byte {
	env := os.Getenv("MORCH_ENV"); if env == "" {
		panic("Environment variable MORCH_ENV is not set!")
	}
	var configurationFilepath string
	if env == local.String() {
		configurationFilepath = "resources/application-dev.yml"
	} else if env == prod.String() {
		configurationFilepath = "/etc/" + application + "/application.yml"
	} else {
		panic(fmt.Errorf("Environment variable MORCH_ENV not set correctly! Environment: %s", env))
	}
	content, err := ioutil.ReadFile(configurationFilepath); if err != nil {
		panic(fmt.Errorf("Configuration could not be read! Error: %v", err))
	}
	return content
}
