package monitoring

import (
	"bitbucket.org/stavrosicsd/morch/common"
	"strings"
)

func ToCommaSeparatedAssets(rules []IToolRule) string {
	var assets []string
	for _, rule := range rules {
		asset := string(rule.Asset())
		if common.SpliceNotContains(assets, asset) {
			assets = append(assets, asset)
		}
	}
	return strings.Join(assets, ",")
}

func JoinTags(tags []Tag) string {
	var stringTags []string
	for _, tag := range tags {
		stringTags = append(stringTags, string(tag))
	}
	return strings.Join(stringTags, ", ")
}