package monitoring

import (
	"bytes"
	"github.com/pkg/errors"
	"strings"
	"text/template"
)

func GetTemplatedResult(tmplt string, data interface{}) (string, error) {
	tplFuncMap := template.FuncMap{
		"add": func(i int, j int) int {
			return i + j
		},
		"split":   strings.Split,
		"joinTags":    JoinTags,
		"toCommaSeparatedAssets": ToCommaSeparatedAssets,
		"ToLower": strings.ToLower,
		"ToUpper": strings.ToUpper,
	}

	template, err := template.New("tmp").Funcs(tplFuncMap).Parse(tmplt); if err != nil {
		return "", errors.New("Failed to parse the template!")
	}
	var buffer bytes.Buffer
	if err := template.Execute(&buffer, data); err != nil {
		return "", errors.New("Failed to fill the template!")
	}
	return buffer.String(), nil
}