package monitoring

type Asset string
type Tool string
type Service string
type Tag string

type IToolRule interface {
	Asset() Asset
	Tool()  Tool
}

type ToolRule struct {
	Asset 		Asset
	Tool 		Tool
	Parameters	map[string]interface{}
}

type Configuration struct {
	ConfigurationList 	[]ConfigurationEntry 	`json:"configurationList"`
	LogFileMounts 		[]string   				`json:"logFileMounts"`
}

type ConfigurationEntry struct {
	Type          ConfigType `json:"type" binding:"required"`
	Content       string     `json:"content" binding:"required"`
	Target        string     `json:"target" binding:"required"`
}

type ConfigType int

const (
	FILE ConfigType = iota
	SCRIPT
)

// TODO: Refactor struct names
type Rule struct {
	Asset							`json:"asset" binding:"required"`
	Tags		[]Tag				`json:"tags" binding:"required"`
	Parameters	map[string]string 	`json:"parameters" binding:"required"`
}

type TaggedRule struct {
	Asset 		Asset				`json:"asset"`
	Tool 		Tool	      		`json:"tool"`
	Service		Service				`json:"service"`
	Tags 		[]Tag				`json:"tags,omitempty"`
	Parameters	map[string]string  	`json:"parameters"`
}

type ServiceRulesGroupedByTool struct {
	Tool         				`json:"tool"`
	ServiceRules []ServiceRule 	`json:"serviceRules"`
}

type ServiceRule struct {
	Service	Service		`json:"service"`
	Rules 	[]Rule		`json:"rules"`
}

func (service *Service) String() string {
	return string(*service)
}

func (asset *Asset) String() string {
	return string(*asset)
}

func (tool *Tool) String() string {
	return string(*tool)
}