package common

import "fmt"

const(
	NoType = ErrorType(iota)
	BadRequest
	InternalServerError
)
type ErrorType uint

type MorchError struct {
	ErrorType ErrorType
	Message   string
	Context   map[string]string
}

func (morchError MorchError) Error() string {
	return fmt.Errorf(morchError.Message+ " Context: %v", morchError.Context).Error()
}

func (morchError MorchError) Type() ErrorType {
	return morchError.ErrorType;
}

func GetErrorType(err error) ErrorType {
	if morchError, ok := err.(MorchError); ok {
		return morchError.ErrorType
	}
	return NoType
}