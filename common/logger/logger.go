package logger

import (
	"fmt"
	"github.com/gin-gonic/gin"
	log "github.com/sirupsen/logrus"
	"io"
	"os"
	"time"
)

const BASE_PATH string = "/var/log"
const TIMESTAMP_FORMAT string = "2006-01-02 15:04:05"
const DATE_FORMAT string = "20060102"
const LOG_EXTENSION string = ".log"

func Init(application string) *os.File {
	file, err := os.OpenFile(getLogFilepath(application), os.O_WRONLY | os.O_APPEND | os.O_CREATE, 0644); if err != nil {
		panic(fmt.Errorf("Failed to initialize logger! Error: %v",  err))
	}
	configureLogger(file)
	return file
}

func configureLogger(file *os.File) {
	log.SetOutput(file)
	Formatter := new(log.TextFormatter)
	Formatter.TimestampFormat = TIMESTAMP_FORMAT
	Formatter.FullTimestamp = true
	log.SetFormatter(Formatter)
	log.SetLevel(log.InfoLevel)
	gin.DefaultWriter = io.MultiWriter(file)
}

func getLogFilepath(application string) string {
	applicationLogPath := BASE_PATH + "/" + application
	if _, err := os.Stat(applicationLogPath); os.IsNotExist(err) {
		err := os.Mkdir(applicationLogPath, 0644); if err != nil {
			panic(fmt.Errorf("Failed create folder for logging! Error: %v",  err))
		}
	}
	return applicationLogPath + "/" + application + "_" + time.Now().Format(DATE_FORMAT) + LOG_EXTENSION
}