#!/bin/bash
export MORCH_ENV=prod
export DOCKER_HUB_ACCOUNT=stavrostum

build_subprojects() {
    for directory in */ ; do
        cd $directory &&
        if [ -f build.sh ]; then
            ./build.sh
        fi &&
        cd ..
    done
}