package plugin

import (
	"errors"
	"strings"
)

func ValidateParams(params map[string]string, paramTemplates map[string]Template) error {
	var errorStrings []string
	for name, value := range params {
		if template, exists := paramTemplates[name]; !exists {
			errorStrings = append(errorStrings, name + " parameter not found!")
		} else if !template.Template.ValidateParameter(value) {
			errorStrings = append(errorStrings, name + " value is invalid!")
		}
	}
	if len(errorStrings) > 0 {
		return errors.New(strings.Join(errorStrings, " "))
	}
	return nil
}

func GetParameters(paramTemplates map[string]Template) []Parameter {
	params := []Parameter{}
	for name, template := range paramTemplates {
		params = append(params, Parameter{name, template})
	}
	return params
}

func GetElementMaxLength(elements []string) int {
	var maxLength = 0;
	for _, element := range elements {
		elementLength := len(element)
		if elementLength > maxLength {
			maxLength = elementLength
		}
	}
	return maxLength
}