package plugin

import (
	"bitbucket.org/stavrosicsd/morch/common"
	"bitbucket.org/stavrosicsd/morch/common/monitoring"
	"regexp"
	"strconv"
	"strings"
)

type PluginType int

type Plugin interface {
	GetSpecification() []Specification
	GetMonitoringConfiguration(rules []monitoring.Rule) (monitoring.Configuration, error)
	GetLoggingConfiguration(serviceRules []monitoring.ServiceRule) (string, string, error)
	GetTool() monitoring.Tool
}

type RuleSpecification interface {
	Asset() 		monitoring.Asset
	Tool()  		monitoring.Tool
	Parameters() 	[]Parameter
}

type Specification struct {
	Asset      monitoring.Asset 	`json:"asset" binding:"required"`
	Tool       monitoring.Tool  	`json:"tool" binding:"required"`
	Parameters []Parameter      	`json:"parameters" binding:"required"`
}

func ToSpecification(ruleSpecification RuleSpecification) Specification {
	return Specification{ruleSpecification.Asset(), ruleSpecification.Tool(), ruleSpecification.Parameters()}
}

type Parameter struct {
	Name     string	`json:"name" binding:"required"`
	Template
}

type ParameterTemplate interface {
	ValidateParameter(param interface{}) bool
}

type Template struct {
	Required        bool              	`json:"required" binding:"required"`
	Position        int               	`json:"position" binding:"required"`
	MultipleAllowed bool              	`json:"multipleAllowed" binding:"required"`
	MaxLength       int             	`json:"maxLength" binding:"required"`
	Template        ParameterTemplate 	`json:"template" binding:"required"`
}

type enumParameterTemplate struct {
	Type string		`json:"type" binding:"required"`
	Values []string `json:"values" binding:"required"`
}

func NewEnumParameterTemplate(values []string) ParameterTemplate {
	return enumParameterTemplate {"enum", values}
}

func (ept enumParameterTemplate) ValidateParameter(param interface{}) bool {
	return common.SpliceContains(ept.Values, param.(string))
}

type stringParameterTemplate struct {
	Type string		`json:"type" binding:"required"`
	Regex string 	`json:"regex" binding:"required"`
}

func NewStringParameterTemplate(regex string) ParameterTemplate {
	return stringParameterTemplate {"string", regex}
}

func (spt stringParameterTemplate) ValidateParameter(param interface{}) bool {
	return regexp.MustCompile(spt.Regex).MatchString(param.(string))
}

type conditionalParameterTemplate struct {
	Type       string   `json:"type" binding:"required"`
	Keys       []string `json:"keys" binding:"required"`
	Operators  []string `json:"operators" binding:"required"`
	ValueRegex string   `json:"value" binding:"required"`
}

func NewConditionalParameterTemplate(keys []string, operators []string, valueRegex string) ParameterTemplate {
	return conditionalParameterTemplate{"conditional", keys, operators, valueRegex}
}

func (fpt conditionalParameterTemplate) ValidateParameter(param interface{}) bool {
	field := param.(string)
	//TODO: please use pattern matching better
	return common.StringContains(field, fpt.Keys) && common.StringContains(field, fpt.Operators)
}

type listParameterTemplate struct {
	Values []string `json:"values" binding:"required"`
}

func NewListParameterTemplate(values []string) ParameterTemplate {
	return listParameterTemplate {values}
}

func (lpt listParameterTemplate) ValidateParameter(param interface{}) bool {
	stringSplit := strings.Split(param.(string),",")
	for _, element := range stringSplit {
		if(common.SpliceNotContains(lpt.Values, element)) {
			return false;
		}
	}
	return true
}

func GetMonitoringConfiguration(toolTemplate string, rules []monitoring.IToolRule, configType monitoring.ConfigType,
	folder string, fileName string, extension string, logFileMounts []string) (monitoring.Configuration, error)  {
	batch := 1000
	batchNumber := 0
	var configurationList []monitoring.ConfigurationEntry
	for i := 0; i < len(rules); i += batch {
		j := i + batch
		if j > len(rules) {
			j = len(rules)
		}
		templatedResult, err := monitoring.GetTemplatedResult(toolTemplate, rules[i:j]); if err != nil {
			return monitoring.Configuration{}, err
		}
		configurationList = append(configurationList,
			monitoring.ConfigurationEntry{configType, templatedResult,
				folder + "/" + fileName + "-" + strconv.Itoa(batchNumber) + extension})
		batchNumber++;
	}
	return monitoring.Configuration{configurationList, logFileMounts}, nil
}

func GetLoggingConfiguration(toolTemplate string, taggedServiceRules []monitoring.ServiceRule) (string, error)  {
	templatedResult, err := monitoring.GetTemplatedResult(toolTemplate, taggedServiceRules); if err != nil {
		return "", err
	}
	return templatedResult, nil
}