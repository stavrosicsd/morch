package morch_http

import (
	"bitbucket.org/stavrosicsd/morch/common"
	"bytes"
	"crypto/tls"
	"fmt"
	"github.com/gin-gonic/gin"
	log "github.com/sirupsen/logrus"
	"io/ioutil"
	"net/http"
	"strconv"

)

func HandleResponse(c *gin.Context, responseBody interface{}, errors []error) {
	if len(errors) > 0 && errors[0] != nil {
		status := common.BadRequest
		for _, error := range errors {
			log.WithFields(log.Fields{"error" : error}).Panic("Failed to apply the monitoring configuration!")
			if(common.GetErrorType(error) == common.InternalServerError) {
				status = common.InternalServerError
			}
		}
		switch status {
			case common.BadRequest:
				c.JSON(http.StatusBadRequest, errors)
			default:
				c.JSON(http.StatusInternalServerError, errors)
		}
	} else {
		c.JSON(http.StatusOK, responseBody)
	}
}

func DoGet(host string, port int, resource string, requestBody []byte) ([]byte, error) {
	return request(host, port, resource, http.MethodGet, requestBody)
}

func DoPost(host string, port int, resource string, requestBody []byte) ([]byte, error) {
	return request(host, port, resource, http.MethodPost, requestBody)
}

func request(host string, port int, resource string, method string, requestBody []byte) ([]byte, error) {
	url := "https://" + host + ":" + strconv.Itoa(port) + resource
	req, err := http.NewRequest(method, url, bytes.NewBuffer(requestBody)); if err != nil {
		return nil, fmt.Errorf("Http request generation failed! Resource: %s Error: %v", resource, err)
	}
	// TODO: Handle with certificates
	req.Header.Set("Content-Type", "application/json")
	client := http.Client{
		Transport: &http.Transport{
			TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
		},
	}
	resp, err := client.Do(req); if err != nil {
		return nil, fmt.Errorf("Http request failed! Error: %v", err)
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		//TODO Fix error formatting to correct types (e.g. %s etc) and [] for properties
		return nil, fmt.Errorf("Http request failed! Resource: %s Status: %v", resource, resp.StatusCode)
	}
	body, err := ioutil.ReadAll(resp.Body); if err != nil {
		return nil, fmt.Errorf("Failed to read response body! Resource: %s Status: %v", resource, resp.StatusCode)
	}
	return body, nil
}