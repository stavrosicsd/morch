Local development

Prerequisites:
1) Docker installed and swarm mode set
2) Auditd installed

1) Setup environment variable: export MORCH_ENV=local
2) Create a folder that belongs to the current user for logging:
sudo mkdir /var/log/configuration_engine && sudo chown stavros:stavros /var/log/configuration_engine
3) Export your dockerhub account: export DOCKER_HUB_ACCOUNT='your docker hub account'
