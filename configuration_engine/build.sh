#!/bin/bash

build_plugins() {
    source ../common/common.sh &&
    project_dir=$(pwd) &&
    cd ../plugins &&
    build_subprojects &&
    cd $project_dir &&
    mkdir -p plugins &&
    mv -v ../plugins/*/target/*.so plugins &&
    rm -rf ../plugins/*/target
}

build_plugins &&
go build -o target/configuration_engine configuration_engine.go &&
docker build -t configuration_engine . &&
docker image tag configuration_engine $DOCKER_HUB_ACCOUNT/configuration_engine &&
docker image push $DOCKER_HUB_ACCOUNT/configuration_engine &&
rm -rf target