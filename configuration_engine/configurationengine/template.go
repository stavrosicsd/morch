package configurationengine

var logConfigurationBase =
`output.elasticsearch:
  hosts: ['elasticsearch:9200']

setup.template:
  name: 'filebeat'
  pattern: 'filebeat-*'
  enabled: false

filebeat.autodiscover:
  providers:
  - type: docker
    templates:`
