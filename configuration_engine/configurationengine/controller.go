package configurationengine

import (
	"bitbucket.org/stavrosicsd/morch/common/monitoring"
	"github.com/gin-gonic/gin"
	log "github.com/sirupsen/logrus"
	"net/http"
)

func ConfigurationEngineController(router *gin.Engine) {
	router.GET("/monitoring/specification", func(c *gin.Context) {
		c.JSON(http.StatusOK, getSpecification())
	})

	router.GET("/monitoring/configuration/:tool", func(c *gin.Context) {
		var rules []monitoring.Rule
		handleRequest(&rules, c)
		configuration, err := getMonitoringConfiguration(monitoring.Tool(c.Param("tool")), rules);
		handleResponse(configuration, err, c)
	})

	router.GET("/logging/configuration", func(c *gin.Context) {
		var serviceRulesGroupedByTool []monitoring.ServiceRulesGroupedByTool
		handleRequest(&serviceRulesGroupedByTool, c)
		logConfiguration, err := getLoggingConfiguration(serviceRulesGroupedByTool);
		handleResponse(logConfiguration, err, c)
	})
}

// TODO: Refactor to the common error handling
func handleRequest(request interface{}, c *gin.Context) {
	err := c.BindJSON(request); if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
	}
	log.WithFields(log.Fields{"body": request}).Info("Request received!")
}

func handleResponse(body interface{}, err error, c *gin.Context) {
	if err != nil {
		c.JSON(http.StatusBadRequest, err.Error())
		log.WithFields(log.Fields{"error": err.Error()}).Info("Error occured!")
	} else {
		c.JSON(http.StatusOK, body)
	}
}