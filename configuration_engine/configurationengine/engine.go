package configurationengine

import (
	"bitbucket.org/stavrosicsd/morch/common/monitoring"
	"bitbucket.org/stavrosicsd/morch/common/plugin"
	"bitbucket.org/stavrosicsd/morch/configuration_engine/config"
	"fmt"
	"github.com/pkg/errors"
	log "github.com/sirupsen/logrus"
	"io/ioutil"
	"path/filepath"
	goPlugin "plugin"
	"strconv"
	"strings"
)

var plugins = getPlugins()
var specificationList []plugin.Specification

func getPlugins() map[monitoring.Tool]plugin.Plugin {
	pluginFiles, err := ioutil.ReadDir(config.Conf.Plugins.Path); if err != nil {
		panic(fmt.Errorf("Could list plugins! Error: %v", err))
	}
	plugins := make(map[monitoring.Tool]plugin.Plugin)
	for _, pluginFile := range pluginFiles {
		pluginFile := pluginFile.Name()
		if strings.HasSuffix(pluginFile, ".so") {
			pluginName := strings.TrimSuffix(pluginFile, filepath.Ext(pluginFile))
			plugin, err := getPlugin(pluginName)
			if err != nil {
				log.WithFields(log.Fields{"plugin": pluginName, "error": err}).Error("Plugin could not be loaded!")
			} else {
				plugins[monitoring.Tool(pluginName)] = plugin
				log.WithFields(log.Fields{"plugin": pluginName}).Info("Plugin is loaded!")
				specificationList = append(specificationList, plugin.GetSpecification()...)
			}
		} else {
			log.WithFields(log.Fields{"file": pluginFile}).Error("Ignored non plugin file!")
		}
	}
	if len(plugins) == 0 {
		panic(errors.New("No plugins found!"))
	}
	return plugins
}

func getPlugin(pluginName string) (plugin.Plugin, error) {
	pluginSo, err := goPlugin.Open(config.Conf.Plugins.Path + "/" + pluginName + ".so"); if err != nil {
		return nil, fmt.Errorf("Could not load plugin shared object! Error: %v", err)
	}
	pluginSymbol, err := pluginSo.Lookup("Plugin"); if err != nil {
		return nil, fmt.Errorf("Could not load plugin symbol! Error: %v", err)
	}
	plugin, ok := pluginSymbol.(plugin.Plugin); if !ok {
		return nil, errors.New("Unexpected type from plugin symbol!")
	}
	return plugin, nil
}

func getSpecification() []plugin.Specification {
	return specificationList
}

func getMonitoringConfiguration(tool monitoring.Tool, rules []monitoring.Rule) (monitoring.Configuration, error) {
	plugin := plugins[tool]; if plugin == nil {
		return monitoring.Configuration{}, fmt.Errorf("No plugin found for tool %s", tool)
	}
	return plugin.GetMonitoringConfiguration(rules)
}



func getLoggingConfiguration(serviceRulesGroupedByTool []monitoring.ServiceRulesGroupedByTool) (monitoring.Configuration, error) {
	completeLogging, logFileMountsList, err := getCompleteLoggingConfiguration(serviceRulesGroupedByTool); if err != nil {
		return monitoring.Configuration{}, err
	}
	completeConfigBytes := []byte(completeLogging)
	batch := 500000 //i.e. 500kb, 512kb is the secret limit
	batchNumber := 0
	var configurationList []monitoring.ConfigurationEntry
	for i := 0; i < len(completeConfigBytes); i += batch {
		j := i + batch
		if j > len(completeConfigBytes) {
			j = len(completeConfigBytes)
		}
		configurationList = append(configurationList,
			monitoring.ConfigurationEntry{monitoring.FILE, string(completeConfigBytes[i:j]),
				"/tmp/morch-" + strconv.Itoa(batchNumber)})
		batchNumber++;
	}
	return monitoring.Configuration{configurationList, logFileMountsList}, nil
}

func getCompleteLoggingConfiguration(serviceRulesGroupedByTool []monitoring.ServiceRulesGroupedByTool) (string, []string, error) {
	var logConfigurationList []string
	var logFileMountsList []string
	logConfigurationList = append(logConfigurationList, logConfigurationBase)
	for _, serviceRulesForTool := range serviceRulesGroupedByTool {
		tool := serviceRulesForTool.Tool
		logConfiguration, logFileMount, err := plugins[tool].GetLoggingConfiguration(serviceRulesForTool.ServiceRules); if err != nil {
			return "", []string{}, fmt.Errorf("Building log configuration failed for tool %s Error %v", tool, err)
		}
		logConfigurationList = append(logConfigurationList, logConfiguration)
		logFileMountsList = append(logFileMountsList, logFileMount)
	}
	return strings.Join(logConfigurationList, ""), logFileMountsList, nil
}