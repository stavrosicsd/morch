package router

import (
	"bitbucket.org/stavrosicsd/morch/configuration_engine/config"
	"bitbucket.org/stavrosicsd/morch/configuration_engine/configurationengine"
	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
)

func Router() *gin.Engine {
	gin.SetMode(config.Conf.Gin.Mode)
	router := gin.Default()
	router.Use(cors.Default())
	configurationengine.ConfigurationEngineController(router)
	return router
}
