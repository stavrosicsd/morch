package main

import (
	"bitbucket.org/stavrosicsd/morch/configuration_engine/config"
	"bitbucket.org/stavrosicsd/morch/common/logger"
	"bitbucket.org/stavrosicsd/morch/configuration_engine/router"
	"fmt"
	"net/http"
	"strconv"
)

func main() {
	defer logger.Init(config.Conf.Application.Name).Close()
	port := strconv.Itoa(config.Conf.Port)
	err := http.ListenAndServeTLS(":" + port, config.Conf.Cert, config.Conf.Key, router.Router()); if err != nil {
		panic(fmt.Errorf("Model manager initialization failed! Error: %v", err))
	}
}
