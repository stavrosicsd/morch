#!/bin/sh
files=/tmp/morch-*
ls -l $files
if ls $files 1> /dev/null 2>&1; then
    cat $files > /usr/share/filebeat/filebeat.yml
    rm -r $files
fi
/usr/local/bin/docker-entrypoint -e