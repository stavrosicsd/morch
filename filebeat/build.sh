#!/bin/bash
docker build -t filebeat . &&
docker image tag filebeat $DOCKER_HUB_ACCOUNT/filebeat &&
docker image push $DOCKER_HUB_ACCOUNT/filebeat