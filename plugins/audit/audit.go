package main

import (
	"bitbucket.org/stavrosicsd/morch/common/monitoring"
	"bitbucket.org/stavrosicsd/morch/common/plugin"
)

type auditPlugin string

var Plugin auditPlugin

const TOOL  = "audit"
const FOLDER = "/tmp"
const FILENAME = "morch"
const EXTENSION = ".rules"
const FILE_ASSET = "file"
const SYSCALL_ASSET = "syscall"
const LOG_FILEPATH = "/var/log/audit/audit.log"

func (a auditPlugin) GetSpecification() []plugin.Specification {
	return []plugin.Specification{
		plugin.ToSpecification(fileRule{}),
		plugin.ToSpecification(syscallRule{}),
	}
}

func (a auditPlugin) GetMonitoringConfiguration(rules []monitoring.Rule) (monitoring.Configuration, error)  {
	toolRules, err := validateAndGetRules(rules); if err != nil {
		return monitoring.Configuration{}, err
	}
	return plugin.GetMonitoringConfiguration(auditTemplate, toolRules, monitoring.FILE, FOLDER, FILENAME, EXTENSION, []string{})
}

func validateAndGetRules(rules []monitoring.Rule) ([]monitoring.IToolRule, error) {
	var auditRules []monitoring.IToolRule
	//fileRules := make(map[string]fileRule)
	for _, rule := range rules {
		params := rule.Parameters
		if rule.Asset == FILE_ASSET {
			fileRule, err := NewFileRule(params); if err != nil {
				return nil, err
			}
			//filepath := fileRule.Filepath
			//fileRuleWithSameFilepath, exists := fileRules[filepath]; if exists {
			//	return nil, common.MorchError{common.BadRequest,
			//	"Audit rule for the same filepath already exists!",
			//		map[string]string{"existingFileRule": fileRuleWithSameFilepath.String(), "newFileRule": fileRule.String()},
			//			}
			//}
			//fileRules[filepath] = fileRule
			auditRules = append(auditRules, fileRule)
		} else if rule.Asset == SYSCALL_ASSET {
			syscallRule, err := NewSyscallRule(params); if err != nil {
				return nil, err
			}
			auditRules = append(auditRules, syscallRule)
		}
	}
	return auditRules, nil
}

func (a auditPlugin) GetLoggingConfiguration(serviceRules []monitoring.ServiceRule) (string, string, error) {
	configuration, err := plugin.GetLoggingConfiguration(auditLogTemplate, serviceRules)
	return configuration, LOG_FILEPATH, err
}

func (a auditPlugin) GetTool() monitoring.Tool {
	return TOOL
}