Prerequisites:
1) Audit is installed and auditd daemon is running
2) Docker swarm cluster is setup and running

To enable the audit container restart the audit service on the host we need to:
1) Append the id_rsa.pub public key to the list of authorized keys so that the audit container can ssh to host:
sudo cat resources/id_rsa.pub >> /root/.ssh/authorized_keys