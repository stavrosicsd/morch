#!/bin/bash
go build -buildmode=plugin -o target/audit.so *.go &&
docker build --add-host "docker.for.localhost:$(ip -4 addr show docker0 | grep -Po 'inet \K[\d.]+')" -t audit . &&
docker image tag audit $DOCKER_HUB_ACCOUNT/audit &&
docker image push $DOCKER_HUB_ACCOUNT/audit