package main

import (
	"bitbucket.org/stavrosicsd/morch/common/monitoring"
	"bitbucket.org/stavrosicsd/morch/common/plugin"
	"fmt"
)

type fileRule struct {
	Filepath string
	Permissions string
	Key string
}

func (fr fileRule) String() string {
	return fmt.Sprintf("[filepath=%s, permissions=%s, key=%s]", fr.Filepath, fr.Permissions, fr.Key)
}

func (f fileRule) Asset() monitoring.Asset {
	return "file"
}

func (f fileRule) Tool() monitoring.Tool {
	return "audit"
}

var permissionsList = []string {"r", "w", "x", "a", "rw", "rx", "ra", "wx", "wa", "xa", "rwx", "rwa", "rxa", "wxa", "rwxa",}

var fileRuleTemplates = map[string]plugin.Template{
	"filepath":    {true, 1, false, 50, plugin.NewStringParameterTemplate("^(/[^/ ]*)+/?$")},
	"permissions": {true, 2, false, plugin.GetElementMaxLength(permissionsList), plugin.NewEnumParameterTemplate(permissionsList)},
	"key":         {true, 3, false, 30, plugin.NewStringParameterTemplate("^[a-zA-Z_$][a-zA-Z_$0-9]{0,30}$")},
}

func (f fileRule) Parameters() []plugin.Parameter {
	return plugin.GetParameters(fileRuleTemplates)
}

func NewFileRule(params map[string]string) (fileRule, error) {
	err := plugin.ValidateParams(params, fileRuleTemplates); if err != nil {
		return fileRule{}, err
	}
	return fileRule{params["filepath"], params["permissions"], params["key"]}, nil
}
