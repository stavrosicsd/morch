package main

var auditTemplate =
`{{range .}}
{{- if eq .Asset "file"}}
-w {{.Filepath}} -p {{.Permissions}} -k {{.Key}}
{{- else if eq .Asset "syscall"}}
-a {{.Action}},{{.Filter}} {{.Field}} {{.Syscall}} -k {{.Key}}
{{- end}}
{{- end}}`

var auditLogTemplate =
`{{- range .}}
    - condition.contains:
        docker.container.labels.com.docker.swarm.service.name: {{.Service}}
      config:
      - module: auditd
        log:
          var.paths: ["/var/log/audit/audit.log"]
          input:
            multiline.pattern: "^type=EXECVE.*"
            multiline.negate: false
            multiline.match: after
            include_lines: [
		    {{- range .Rules}}
              {{- if eq .Asset "file"}}
              "^type=SYSCALL.*key=\"{{index .Parameters "key"}}\"$",
              {{- else if eq .Asset "syscall"}}
              {{- $args := index .Parameters "args"}}
              {{- $arguments := split $args " "}}
              "^type=(SYSCALL.*\ntype=EXECVE.*{{- range $i, $arg := $arguments}} a{{add $i 1}}=\"{{$arg}}\"{{- end}}|{{index .Parameters "syscall-1"}}.*{{index .Parameters "field-1-key"}}=\"{{index .Parameters "field-1-value"}}\").*$",
              {{- end}}
		    {{- end}}
            ]
            tags: [morch]
            processors:
		    {{- range .Rules}}
            - add_tags:
                when:
                  {{- if eq .Asset "file"}}
                  regexp:
                    message: "^type=SYSCALL.*key=\"{{index .Parameters "key"}}\"$"
                  {{- else if eq .Asset "syscall"}}
                  regexp:
                    {{- $args := index .Parameters "args"}}
                    {{- $arguments := split $args " "}}
                    message: "^type=(SYSCALL.*\ntype=EXECVE.*{{- range $i, $arg := $arguments}} a{{add $i 1}}=\"{{$arg}}\"{{- end}}|{{index .Parameters "syscall-1"}}.*{{index .Parameters "field-1-key"}}=\"{{index .Parameters "field-1-value"}}\").*$"
                  {{- end}}
                tags: [{{joinTags .Tags}}]
            {{- if eq .Asset "syscall"}}
            - add_fields:
                when:
                  regexp:
                    {{- $args := index .Parameters "args"}}
                    {{- $arguments := split $args " "}}
                    message: "^type=SYSCALL.*\ntype=EXECVE.*{{- range $i, $arg := $arguments}} a{{add $i 1}}=\"{{$arg}}\"{{- end}}.*$"
                fields:
                  args:{{- range $i, $arg := $arguments}} {{$arg}}{{- end}}
            {{- end}}
		    {{- end}}
{{- end}}`