#!/bin/ash
CONFIGURATION_TEMPORARY_PATH=/tmp
CONFIGURATION_TARGET_PATH=/etc/audit/rules.d
if [ -d $CONFIGURATION_TEMPORARY_PATH ]; then
    rm -r $CONFIGURATION_TARGET_PATH
    mv -v $CONFIGURATION_TEMPORARY_PATH/* $CONFIGURATION_TARGET_PATH
fi
ssh -oStrictHostKeyChecking=no root@localhost service auditd restart
tail -f /dev/null