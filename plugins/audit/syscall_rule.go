package main

import (
	"bitbucket.org/stavrosicsd/morch/common/monitoring"
	"bitbucket.org/stavrosicsd/morch/common/plugin"
	"strings"
)

type syscallRule struct {
	Action string
	Filter string
	Field string
	Syscall string
	Key string
	LogFilter string
}

func (s syscallRule) Asset() monitoring.Asset {
	return "syscall"
}

func (s syscallRule) Tool() monitoring.Tool {
	return "audit"
}

var syscallTemplates = map[string]plugin.Template{
	"action":  {true, 1, false, plugin.GetElementMaxLength(actions), plugin.NewEnumParameterTemplate(actions)},
	"filter":  {true, 2, false,plugin.GetElementMaxLength(filters), plugin.NewEnumParameterTemplate(filters)},
	"field":  {false, 3, true, 20, plugin.NewConditionalParameterTemplate(fields, operators, ".*")},
	"syscall":  {false, 4, true, plugin.GetElementMaxLength(syscalls), plugin.NewEnumParameterTemplate(syscalls)},
	"key":  {true, 5, false, 30, plugin.NewStringParameterTemplate("^[a-zA-Z_$][a-zA-Z_$0-9]{0,30}$")},
	"args":  {true, 6, false, 40, plugin.NewStringParameterTemplate(".*")},
}

func (s syscallRule) Parameters() []plugin.Parameter {
	return plugin.GetParameters(syscallTemplates)
}

func NewSyscallRule(params map[string]string) (syscallRule, error) {
	fieldsStr, syscallsStr := getListParamsAsTemplatedStrings(params)

	// TODO: validation should take into account multiple values of same type, e.g. like field and syscall
	// which can be multiple
	//err := plugin.ValidateParams(params, syscallTemplates); if err != nil {
	//	return syscallRule{}, err
	//}
	return syscallRule{
		params["action"],
		params["filter"],
		fieldsStr,
		syscallsStr,
		params["key"],
		params["args"],
	}, nil
}

func getListParamsAsTemplatedStrings(params map[string]string) (string, string){
	// TODO: this has to happen on the frontend
	fieldsMap := make(map[string]map[string]string)
	var syscallsStr string
	for k, v := range params {
		if strings.HasPrefix(k, "field-") {
			fieldKeySplit := strings.Split(k, "-")
			if fieldsMap[fieldKeySplit[1]] == nil {
				fieldsMap[fieldKeySplit[1]] = make(map[string]string)
			}
			fieldsMap[fieldKeySplit[1]][fieldKeySplit[2]] = v
		}
		if strings.HasPrefix(k, "syscall-") {
			syscallsStr += "-S " + v
		}
	}
	var fieldsStr string
	for key := range fieldsMap {
		fieldsStr += " -F " + fieldsMap[key]["key"] + fieldsMap[key]["operator"] + fieldsMap[key]["value"]
	}
	return fieldsStr, syscallsStr
}

var actions = []string { "always", "never" }
var filters = []string { "task", "exit", "user", "exclude" }
var syscalls = []string {
	"ADD_GROUP",
	"ADD_USER",
	"ANOM_ABEND",
	"ANOM_ACCESS_FS",
	"ANOM_ADD_ACCT",
	"ANOM_AMTU_FAIL",
	"ANOM_CRYPTO_FAIL",
	"ANOM_DEL_ACCT",
	"ANOM_EXEC",
	"ANOM_LOGIN_ACCT",
	"ANOM_LOGIN_FAILURES",
	"ANOM_LOGIN_LOCATION",
	"ANOM_LOGIN_SESSIONS",
	"ANOM_LOGIN_TIME",
	"ANOM_MAX_DAC",
	"ANOM_MAX_MAC",
	"ANOM_MK_EXEC",
	"ANOM_MOD_ACCT",
	"ANOM_PROMISCUOUS",
	"ANOM_RBAC_FAIL",
	"ANOM_RBAC_INTEGRITY_FAIL",
	"ANOM_ROOT_TRANS",
	"AVC",
	"AVC_PATH",
	"BPRM_FCAPS",
	"CAPSET",
	"CHGRP_ID",
	"CHUSER_ID",
	"CONFIG_CHANGE",
	"CRED_ACQ",
	"CRED_DISP",
	"CRED_REFR",
	"CRYPTO_FAILURE_USER",
	"CRYPTO_KEY_USER",
	"CRYPTO_LOGIN",
	"CRYPTO_LOGOUT",
	"CRYPTO_PARAM_CHANGE_USER",
	"CRYPTO_REPLAY_USER",
	"CRYPTO_SESSION",
	"CRYPTO_TEST_USER",
	"CWD",
	"DAC_CHECK",
	"DAEMON_ABORT",
	"DAEMON_ACCEPT",
	"DAEMON_CLOSE",
	"DAEMON_CONFIG",
	"DAEMON_END",
	"DAEMON_RESUME",
	"DAEMON_ROTATE",
	"DAEMON_START",
	"DEL_GROUP",
	"DEL_USER",
	"DEV_ALLOC",
	"DEV_DEALLOC",
	"EOE",
	"EXECVE",
	"FD_PAIR",
	"FS_RELABEL",
	"GRP_AUTH",
	"INTEGRITY_DATA",
	"INTEGRITY_HASH",
	"INTEGRITY_METADATA",
	"INTEGRITY_PCR",
	"INTEGRITY_RULE",
	"INTEGRITY_STATUS",
	"IPC",
	"IPC_SET_PERM",
	"KERNEL",
	"KERNEL_OTHER",
	"LABEL_LEVEL_CHANGE",
	"LABEL_OVERRIDE",
	"LOGIN",
	"MAC_CIPSOV4_ADD",
	"MAC_CIPSOV4_DEL",
	"MAC_CONFIG_CHANGE",
	"MAC_IPSEC_EVENT",
	"MAC_MAP_ADD",
	"MAC_MAP_DEL",
	"MAC_POLICY_LOAD",
	"MAC_STATUS",
	"MAC_UNLBL_ALLOW",
	"MAC_UNLBL_STCADD",
	"MAC_UNLBL_STCDEL",
	"MMAP",
	"MQ_GETSETATTR",
	"MQ_NOTIFY",
	"MQ_OPEN",
	"MQ_SENDRECV",
	"NETFILTER_CFG",
	"NETFILTER_PKT",
	"OBJ_PID",
	"PATH",
	"RESP_ACCT_LOCK",
	"RESP_ACCT_LOCK_TIMED",
	"RESP_ACCT_REMOTE",
	"RESP_ACCT_UNLOCK_TIMED",
	"RESP_ALERT",
	"RESP_ANOMALY",
	"RESP_EXEC",
	"RESP_HALT",
	"RESP_KILL_PROC",
	"RESP_SEBOOL",
	"RESP_SINGLE",
	"RESP_TERM_ACCESS",
	"RESP_TERM_LOCK",
	"ROLE_ASSIGN",
	"ROLE_MODIFY",
	"ROLE_REMOVE",
	"SELINUX_ERR",
	"SERVICE_START",
	"SERVICE_STOP",
	"SOCKADDR",
	"SOCKETCALL",
	"SYSCALL",
	"SYSTEM_BOOT",
	"SYSTEM_RUNLEVEL",
	"SYSTEM_SHUTDOWN",
	"TEST",
	"TRUSTED_APP",
	"TTY",
	"USER_ACCT",
	"USER_AUTH",
	"USER_AVC",
	"USER_CHAUTHTOK",
	"USER_CMD",
	"USER_END",
	"USER_ERR",
	"USER_LABELED_EXPORT",
	"USER_LOGIN",
	"USER_LOGOUT",
	"USER_MAC_POLICY_LOAD",
	"USER_MGMT",
	"USER_ROLE_CHANGE",
	"USER_SELINUX_ERR",
	"USER_START",
	"USER_TTY",
	"USER_UNLABELED_EXPORT",
	"USYS_CONFIG",
	"VIRT_CONTROL",
	"VIRT_MACHINE_ID",
	"VIRT_RESOURCE",
}
var fields = []string {
	"a0",
	"a1",
	"a2",
	"a3",
	"acct",
	"addr",
	"arch",
	"auid",
	"capability",
	"cap_fi",
	"cap_fp",
	"cap_pe",
	"cap_pi",
	"cap_pp",
	"cgroup",
	"cmd",
	"comm",
	"cwd",
	"data",
	"dev",
	"devmajor",
	"devminor",
	"egid",
	"euid",
	"exe",
	"exit",
	"family",
	"filetype",
	"flags",
	"fsgid",
	"fsuid",
	"gid",
	"hostname",
	"icmptype",
	"id",
	"inode",
	"inode_gid",
	"inode_uid",
	"items",
	"key",
	"list",
	"mode",
	"msg",
	"msgtype",
	"name",
	"new-disk",
	"new-mem",
	"new-vcpu",
	"new-net",
	"new_gid",
	"oauid",
	"ocomm",
	"opid",
	"oses",
	"ouid",
	"obj",
	"obj_gid",
	"obj_lev_high",
	"obj_lev_low",
	"obj_role",
	"obj_uid",
	"obj_user",
	"ogid",
	"old-disk",
	"old-mem",
	"old-vcpu",
	"old-net",
	"old_prom",
	"ouid",
	"path",
	"perm",
	"pid",
	"ppid",
	"prom",
	"proto",
	"res",
	"result",
	"saddr",
	"sauid",
	"ses",
	"sgid",
	"sig",
	"subj",
	"subj_clr",
	"subj_role",
	"subj_sen",
	"subj_user",
	"success",
	"suid",
	"syscall",
	"terminal",
	"tty",
	"uid",
	"vm",
}
var operators = []string { "=", "!=", "<", ">", "<=", ">=", "&", "&=" }