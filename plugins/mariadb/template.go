package main

//TODO: For mariadb we don't expect list of rules, please refactor.
var mariadbTemplate =
`[mariadb]
plugin_load=server_audit=server_audit.so
server_audit_logging=ON
server_audit_output_type=FILE
server_audit_file_path=/var/log/mysql/server_audit.log
server_audit_file_rotate_size=1073741824
server_audit_file_rotations=8
log_error=mariadb.err
server_audit_excl_users=root
server_audit_query_log_limit=2048
server_audit_events={{- toCommaSeparatedAssets . }}`

var mariadbLogTemplate =
`{{- range .}}
    - condition.contains:
        docker.container.labels.com.docker.swarm.service.name: {{.Service}}
      config:
        - type: log
          paths:
            - /var/log/mysql/server_audit.log
          include_lines: [
          {{- range .Rules}}
          {{- if eq .Asset "connect"}}
          "^.*{{index .Parameters "host_origin"}},.*CONNECT,.*$",
          {{- else if eq .Asset "query"}}
          "^.*{{index .Parameters "host_origin"}},.*QUERY,{{index .Parameters "database"}},'({{index .Parameters "statement" | ToUpper}}|{{index .Parameters "statement" | ToLower}}).*({{index .Parameters "table" | ToUpper}}|{{index .Parameters "table" | ToLower}}).*$",
          {{- else if eq .Asset "table"}}
          "^.*{{index .Parameters "host_origin"}},.*{{index .Parameters "operation"}},{{index .Parameters "database"}},{{index .Parameters "table"}},$",
          {{- end}}
          {{- end}}
          ]
          exclude_lines: [
            "^.*,root,.*$"
          ]
          tags: [morch]
          processors:
		  {{- range .Rules}}
          - add_tags:
              when:
                {{- if eq .Asset "connect"}}
                regexp:
                  message: "^.*{{index .Parameters "host_origin"}},.*CONNECT,.*$"
                {{- else if eq .Asset "query"}}
                regexp:
                  message: "^.*{{index .Parameters "host_origin"}},.*QUERY,{{index .Parameters "database"}},'({{index .Parameters "statement" | ToUpper}}|{{index .Parameters "statement" | ToLower}}).*({{index .Parameters "table" | ToUpper}}|{{index .Parameters "table" | ToLower}}).*$"
                {{- else if eq .Asset "table"}}
                regexp:
                  message: "^.*{{index .Parameters "host_origin"}},.*{{index .Parameters "operation"}},{{index .Parameters "database"}},{{index .Parameters "table"}},$"
                {{- end}}
              tags: [{{joinTags .Tags}}]
		  {{- end}}
{{- end}}`