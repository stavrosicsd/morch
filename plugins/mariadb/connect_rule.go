package main

import (
	"bitbucket.org/stavrosicsd/morch/common/monitoring"
	"bitbucket.org/stavrosicsd/morch/common/plugin"
)

type connectRule struct {
	HostOrigin string
	Database string
}

func (er connectRule) String() string {
	return "connect"
}

func (e connectRule) Asset() monitoring.Asset {
	return "connect"
}

func (e connectRule) Tool() monitoring.Tool {
	return "mariadb"
}

var connectRuleTemplates = map[string]plugin.Template{
	"host_origin": {false, 1, false, 25,
	plugin.NewStringParameterTemplate(`^$|^(^\w+(?:(?:,\w+)+)|(?:\w+)+$)?$|^(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$|^(([a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9\-]*[a-zA-Z0-9])\.)+([A-Za-z]|[A-Za-z][A-Za-z0-9\-]*[A-Za-z0-9])$`)},
	"database": {true, 2, false, 25, plugin.NewStringParameterTemplate(`(^\w+(?:(?:,\w+)+)|(?:\w+)+$)?`)},
}

func (e connectRule) Parameters() []plugin.Parameter {
	return plugin.GetParameters(connectRuleTemplates)
}

func NewConnectRule(params map[string]string) (connectRule, error) {
	err := plugin.ValidateParams(params, connectRuleTemplates); if err != nil {
		return connectRule{}, err
	}
	return connectRule{params["host_origin"], params["database"]}, nil
}