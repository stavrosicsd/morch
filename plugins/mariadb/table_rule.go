package main

import (
	"bitbucket.org/stavrosicsd/morch/common/monitoring"
	"bitbucket.org/stavrosicsd/morch/common/plugin"
	"fmt"
)

type tableRule struct {
	HostOrigin string
	Database   string
	Table      string
	Operation  string
}

func (tr tableRule) String() string {
	return fmt.Sprintf("[host_origin=%s, database=%s, table=%s, operation=%s]", tr.HostOrigin, tr.Database, tr.Table, tr.Operation)
}

func (tr tableRule) Asset() monitoring.Asset {
	return "table"
}

func (tr tableRule) Tool() monitoring.Tool {
	return "mariadb"
}

var operationsList = []string {"READ", "WRITE"}

var tableRuleTemplates = map[string]plugin.Template{
	"host_origin": {false, 1, false, 25,
	plugin.NewStringParameterTemplate(`^$|^(^\w+(?:(?:,\w+)+)|(?:\w+)+$)?$|^(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$|^(([a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9\-]*[a-zA-Z0-9])\.)+([A-Za-z]|[A-Za-z][A-Za-z0-9\-]*[A-Za-z0-9])$`)},
	"database": {true, 2, false, 25, plugin.NewStringParameterTemplate(`(^\w+(?:(?:,\w+)+)|(?:\w+)+$)?`)},
	"table": {true, 3, false, 25, plugin.NewStringParameterTemplate(`(^\w+(?:(?:,\w+)+)|(?:\w+)+$)?`)},
	"operation": {true, 4, false, 10, plugin.NewEnumParameterTemplate(operationsList)},
}

func (tr tableRule) Parameters() []plugin.Parameter {
	return plugin.GetParameters(tableRuleTemplates)
}

func NewTableRule(params map[string]string) (tableRule, error) {
	err := plugin.ValidateParams(params, tableRuleTemplates); if err != nil {
		return tableRule{}, err
	}
	return tableRule{params["host_origin"], params["database"], params["table"],params["operation"]}, nil
}