package main

import (
	"bitbucket.org/stavrosicsd/morch/common/monitoring"
	"bitbucket.org/stavrosicsd/morch/common/plugin"
)

type mariadbPlugin string

var Plugin mariadbPlugin

const TOOL  = "mariadb"
const FOLDER = "/etc/mysql/mariadb.conf.d"
const FILENAME = "morch"
const EXTENSION = ".cnf"
const CONNECT_ASSET = "connect"
const QUERY_ASSET = "query"
const TABLE_ASSET = "table"
const LOG_FILEPATH = "/var/log/mysql/server_audit.log"

func (m mariadbPlugin) GetSpecification() []plugin.Specification {
	return []plugin.Specification{
		plugin.ToSpecification(connectRule{}),
		plugin.ToSpecification(queryRule{}),
		plugin.ToSpecification(tableRule{}),
	}
}

func (m mariadbPlugin) GetMonitoringConfiguration(rules []monitoring.Rule) (monitoring.Configuration, error)  {
	toolRules, err := validateAndGetRules(rules); if err != nil {
		return monitoring.Configuration{}, err
	}
	return plugin.GetMonitoringConfiguration(mariadbTemplate, toolRules, monitoring.FILE, FOLDER, FILENAME, EXTENSION, []string{LOG_FILEPATH})
}

func validateAndGetRules(rules []monitoring.Rule) ([]monitoring.IToolRule, error) {
	var mariadbRules []monitoring.IToolRule
	for _, rule := range rules {
		params := rule.Parameters
		if rule.Asset == CONNECT_ASSET {
			connectRule, err := NewConnectRule(params); if err != nil {
				return nil, err
			}
			mariadbRules = append(mariadbRules, connectRule)
		} else if rule.Asset == QUERY_ASSET {
			queryRule, err := NewQueryRule(params); if err != nil {
				return nil, err
			}
			mariadbRules = append(mariadbRules, queryRule)
		} else if rule.Asset == TABLE_ASSET {
			tableRule, err := NewTableRule(params); if err != nil {
				return nil, err
			}
			mariadbRules = append(mariadbRules, tableRule)
		}
	}
	return mariadbRules, nil
}

func (m mariadbPlugin) GetLoggingConfiguration(serviceRules []monitoring.ServiceRule) (string, string, error) {
	configuration, err := plugin.GetLoggingConfiguration(mariadbLogTemplate, serviceRules)
	return configuration, LOG_FILEPATH, err
}

func (m mariadbPlugin) GetTool() monitoring.Tool {
	return TOOL
}
