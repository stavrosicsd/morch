package main

import (
	"bitbucket.org/stavrosicsd/morch/common/monitoring"
	"bitbucket.org/stavrosicsd/morch/common/plugin"
	"fmt"
)

type queryRule struct {
	HostOrigin string
	Database   string
	Statement  string
	Table      string
}

func (qr queryRule) String() string {
	return fmt.Sprintf("[host_origin=%s, database=%s, statement=%s, table=%s]", qr.HostOrigin, qr.Database, qr.Statement, qr.Table)
}

func (qr queryRule) Asset() monitoring.Asset {
	return "query"
}

func (qr queryRule) Tool() monitoring.Tool {
	return "mariadb"
}

var expressionList = []string {"SELECT", "INSERT", "UPDATE", "DELETE", "ALTER"}

var queryRuleTemplates = map[string]plugin.Template{
	"host_origin": {false, 1, false, 25,
	plugin.NewStringParameterTemplate(`^$|^(^\w+(?:(?:,\w+)+)|(?:\w+)+$)?$|^(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$|^(([a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9\-]*[a-zA-Z0-9])\.)+([A-Za-z]|[A-Za-z][A-Za-z0-9\-]*[A-Za-z0-9])$`)},
	"database": {true, 2, false, 25, plugin.NewStringParameterTemplate(`(^\w+(?:(?:,\w+)+)|(?:\w+)+$)?`)},
	"statement": {true, 3, false, 10, plugin.NewEnumParameterTemplate(expressionList)},
	"table": {true, 4, false, 25, plugin.NewStringParameterTemplate(`(^\w+(?:(?:,\w+)+)|(?:\w+)+$)?`)},
}

func (qr queryRule) Parameters() []plugin.Parameter {
	return plugin.GetParameters(queryRuleTemplates)
}

func NewQueryRule(params map[string]string) (queryRule, error) {
	err := plugin.ValidateParams(params, queryRuleTemplates); if err != nil {
		return queryRule{}, err
	}
	return queryRule{params["host_origin"], params["database"], params["statement"], params["table"]}, nil
}