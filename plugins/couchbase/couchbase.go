package main

import (
	"bitbucket.org/stavrosicsd/morch/common/monitoring"
	"bitbucket.org/stavrosicsd/morch/common/plugin"
)

type couchbasePlugin string

var Plugin couchbasePlugin

const TOOL  = "couchbase"
const LOG_FILEPATH = "/var/log/couchbase/audit.log"

func (c couchbasePlugin) GetSpecification() []plugin.Specification {
	return []plugin.Specification{}
}

func (c couchbasePlugin) GetMonitoringConfiguration(rules []monitoring.Rule) (monitoring.Configuration, error)  {
	configurationEntry := monitoring.ConfigurationEntry{monitoring.SCRIPT, couchbaseScript, "/audit.sh"}
	return monitoring.Configuration{[]monitoring.ConfigurationEntry{configurationEntry}, []string{LOG_FILEPATH}}, nil
}

func (c couchbasePlugin) GetLoggingConfiguration(serviceRules []monitoring.ServiceRule) (string, string, error) {
	config, err := plugin.GetLoggingConfiguration(couchbaseLogTemplate, serviceRules)
	return config, LOG_FILEPATH, err
}

func (c couchbasePlugin) GetLogFilepath() string {
	return LOG_FILEPATH;
}

func (c couchbasePlugin) GetTool() monitoring.Tool {
	return TOOL
}
