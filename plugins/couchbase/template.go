package main

var couchbaseScript =
`#!/bin/bash
up=$(ps -ef | grep 'runsv couchbase-server' | grep -v grep | awk '{print $9}')
echo "$up" >> res
while [ "$up" != "couchbase-server" ] ; do
  sleep 10
  up=$(ps -ef | grep 'runsv couchbase-server' | grep -v grep | awk '{print $9}')
  echo "$up" >> res
done
curl -X GET -u Administrator:password http://localhost:8091/settings/audit > response.json
if grep -v -q '"auditdEnabled":true' "response.json"; then
  curl -X POST -d auditdEnabled=true -u Administrator:password http://localhost:8091/settings/audit
fi`

var couchbaseScript2 =
`#!/bin/bash
touch /tmp/"$(date)"`

var couchbaseLogTemplate = ``