package router

import (
	"bitbucket.org/stavrosicsd/morch/model_manager/amt"
	"bitbucket.org/stavrosicsd/morch/model_manager/config"
	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"time"
)

func Router() *gin.Engine {
	gin.SetMode(config.Conf.Gin.Mode)
	router := gin.Default()
	router.Use(cors.New(cors.Config{
		AllowMethods:     []string{"OPTIONS, GET, POST, PUT, PATCH, DELETE"},
		AllowHeaders:     []string{"Origin", "Content-Length", "Content-Type", "User-Agent"},
		ExposeHeaders:    []string{"Content-Length"},
		AllowCredentials: true,
		AllowAllOrigins:  true,
		//AllowOriginFunc:  func(origin string) bool { return true },
		MaxAge: 12 * time.Hour,
	}))
	amt.AmtController(router)
	return router
}
