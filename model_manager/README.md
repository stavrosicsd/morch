Local development

1) Setup environment variable: export MORCH_ENV=local
2) Create a folder that belongs to the current user for logging:
sudo mkdir /var/log/model_manager && sudo chown stavros:stavros /var/log/model_manager
3) Run a local elasticsearch instance listening to the default port 9200.
To have elasticsearch running on startup please run sudo systemctl enable elasticsearch.service (on debian based linux distros)
4) Export your dockerhub account: export DOCKER_HUB_ACCOUNT='your docker hub account'

If you use chrome and upon saving the AMTs on the frontend the spinner does not faint after 10 seconds
you have to open the developer tools and most probably you will face an error net::ERR_NETWORK_CHANGED
In that case run you have to disable ipv6 by running the following commands on your terminal:
sysctl -w net.ipv6.conf.all.disable_ipv6=1
sysctl -w net.ipv6.conf.default.disable_ipv6=1