#!/bin/bash
go build -o target/model_manager model_manager.go &&
docker build -t model_manager . &&
docker image tag model_manager $DOCKER_HUB_ACCOUNT/model_manager &&
docker image push $DOCKER_HUB_ACCOUNT/model_manager &&
rm -rf target