package amt

import (
	"bitbucket.org/stavrosicsd/morch/common/monitoring"
	"bitbucket.org/stavrosicsd/morch/common/morch_http"
	"bitbucket.org/stavrosicsd/morch/model_manager/config"
	"context"
	"encoding/json"
	"fmt"
	"github.com/olivere/elastic"
	log "github.com/sirupsen/logrus"
	"strconv"
)

const index = "amt"
const doc = "1"
const mapping = `
{
	"settings":{
		"index.mapping.total_fields.limit": 1000000,
		"index.blocks.read_only_allow_delete": null,
		"number_of_shards": 1,
		"number_of_replicas": 0
	},
	"mappings": {
        "properties": {
			"amts": {
				"properties": {
          			"id": { "type": "text" },
          			"active": { "type": "boolean" },
          			"root": {
            			"properties": {
              				"label": { "type": "text"  },
			  				"type": { "type": "text"  },
              				"refinement": { "type": "text" },
							"rule": { "type": "object" },
			 	 			"children": { "type": "nested" }
						}
		            }
				}
			}
        }
    }
}`

type attackMonitoringTreesWrapper struct {
	AttackMonitoringTrees []attackMonitoringTree `json:"amts"`
}

type attackMonitoringTree struct {
	Id     string `json:"id" binding:"required"`
	Active bool   `json:"active" binding:"required"`
	Root   node   `json:"root" binding:"required"`
}

type node struct {
	Label      	string	`json:"label" binding:"required"`
	Type       	Type    `json:"type" binding:"required"`
	Refinement 	*string	`json:"refinement,omitempty"`
	Rule 	   	*rule	`json:"rule,omitempty"`
	Children   	[]node	`json:"children" binding:"required"`
}

type rule struct {
	Asset 		monitoring.Asset	`json:"asset"`
	Tool 		monitoring.Tool	    `json:"tool"`
	Service		monitoring.Service	`json:"service"`
	Parameters	map[string]string  	`json:"parameters"`
}

type auth struct {
	Username string	`json:"username"`
	Password string	`json:"password"`
}

type Type string

const (
	ATTACK Type = "attack"
	MONITORING = "monitoring"
)

var esClient *elastic.Client
var configurationEngine = config.Conf.ConfigurationEngine
var orchestrator = config.Conf.Orchestrator

func InitializeEsClient()  {
	elasticsearchConf := config.Conf.Elasticsearch
	cli, err := elastic.NewClient(elastic.SetURL("http://" + elasticsearchConf.Host + ":" + strconv.Itoa(elasticsearchConf.Port))); if err != nil {
		panic(fmt.Errorf("Failed to initialize Elasticsearch client! Error: %v",  err))
	}
	esClient = cli
}

func authenticate(username string, password string) bool {
	// TODO: Move credentials to db
	return username == "admin" && password == "ahfaechohMoh8iecu6ei";
}

func getSpecification() interface{} {
	body, err := morch_http.DoGet(configurationEngine.Host, configurationEngine.Port, "/monitoring/specification", []byte{}); if err != nil {
		log.WithFields(log.Fields{"error" : err}).Panic("Failed to fetch the specification!")
	}
	// TODO: Does not unmarshal correctly in []plugin.Specification: the template is null. interface{} is a workaround.
	var specification interface{}
	json.Unmarshal(body, &specification)
	return specification.(interface{})
}

func getServicesForTool(tool monitoring.Tool) []monitoring.Service {
	body, err := morch_http.DoGet(orchestrator.Host, orchestrator.Port, "/tools/" + tool.String() + "/services", []byte{}); if err != nil {
		log.WithFields(log.Fields{"error" : err}).Panic("Failed to fetch the services!")
	}
	var services []monitoring.Service
	json.Unmarshal(body, &services)
	return services
}

func getAttackMonitoringTrees() []attackMonitoringTree {
	exists, err := esClient.Exists().Index(index).Id(doc).Do(context.Background()); if err != nil {
		log.WithFields(log.Fields{"error" : err}).Panic("Error while checking if the index exists!")
	}
	if !exists {
		createIndexWithDocument()
	}
	get, err := esClient.Get().Index(index).Id(doc).Do(context.Background()); if err != nil {
		log.WithFields(log.Fields{"error": err}).Panic("Failed to fetch the document!")
	}
	var attackMonitoringTreesWrapper attackMonitoringTreesWrapper
	if get.Found {
		j, err := json.Marshal(&get.Source);
		if err != nil {
			log.WithFields(log.Fields{"error": err}).Panic("Failed to marshal the source!")
		}
		json.Unmarshal(j, &attackMonitoringTreesWrapper)
	}
	return attackMonitoringTreesWrapper.AttackMonitoringTrees
}

func createIndexWithDocument() {
	_, err := esClient.CreateIndex(index).Do(context.Background())
	if err != nil {
		log.WithFields(log.Fields{"error": err}).Panic("Error while creating the index!")
	}
	amtWrapper := attackMonitoringTreesWrapper{[]attackMonitoringTree{}}
	_, err = esClient.Index().Index(index).Id(doc).BodyJson(amtWrapper).Refresh("wait_for").Do(context.Background())
	if err != nil {
		log.WithFields(log.Fields{"error": err}).Panic("Error while creating the document on the index!")
	}
}

func storeAttackMonitoringTrees(amts []attackMonitoringTree) error {
	applyMonitoringConfiguration(extractMonitoringRules(amts))
	createIndexIfNotExists()
	_, err := esClient.Index().Index(index).Id(doc).BodyJson(attackMonitoringTreesWrapper{amts}).Do(context.Background());
	if err != nil {
		log.WithFields(log.Fields{"amts": amts, "error": err}).Panic("Failed to create the amts!")
	}
	return nil
}

func extractMonitoringRules(amts []attackMonitoringTree) []monitoring.TaggedRule {
	var monitoringRules []monitoring.TaggedRule
	for _, amt := range amts {
		if(amt.Active) {
			appendMonitoringRules(&monitoringRules, amt.Root.Children, amt.Root.Label)
		}
	}
	return monitoringRules
}

func applyMonitoringConfiguration(rules []monitoring.TaggedRule) {
	fmt.Printf("#ofMonitoringNodes=%d\n", len(rules))
	marshaledRules, err := json.Marshal(rules); if err != nil {
		log.WithFields(log.Fields{"error" : err}).Panic("Failed to marchal the rules!")
	}
	if(len(marshaledRules) == 0) {
		return
	}
	_, err = morch_http.DoPost(orchestrator.Host, orchestrator.Port, "/configure", marshaledRules); if err != nil {
		log.WithFields(log.Fields{"error" : err}).Panic("Failed to apply the requested monitoring configuration!")
	}
}

func appendMonitoringRules(monitoringRules *[]monitoring.TaggedRule, nodes []node, attackGoal string) {
	for _, node := range nodes {
		if len(node.Children) == 0 && node.Type == MONITORING {
			nodeRule := node.Rule
			*monitoringRules = append(*monitoringRules, monitoring.TaggedRule{nodeRule.Asset, nodeRule.Tool,
			nodeRule.Service, []monitoring.Tag{monitoring.Tag(attackGoal), monitoring.Tag(node.Label)},nodeRule.Parameters})
		} else {
			appendMonitoringRules(monitoringRules, node.Children, attackGoal)
		}
	}
}

func createIndexIfNotExists() {
	ctx := context.Background()
	exists, err := esClient.IndexExists(index).Do(ctx); if err != nil {
		log.WithFields(log.Fields{"error": err}).Panic("Failed while checking for index existence!")
	}
	if !exists {
		createIndex, err := esClient.CreateIndex(index).BodyString(mapping).Do(ctx); if err != nil {
			log.WithFields(log.Fields{"error": err}).Panic("Failed to create the index!")
		}
		if !createIndex.Acknowledged {
			log.WithFields(log.Fields{"error": err}).Panic("Failed to Acknowledged index creation!")
		}
	}
}