package amt

import (
	"bitbucket.org/stavrosicsd/morch/common/monitoring"
	"bytes"
	"encoding/json"
	"github.com/gin-gonic/gin"
	"github.com/opentracing/opentracing-go/log"
	"io"
	"net/http"
)

func AmtController(router *gin.Engine) {

	router.POST("/auth", func(c *gin.Context) {
		var auth auth
		err := c.BindJSON(&auth); if err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
			return
		}
		if authenticate(auth.Username, auth.Password) {
			c.JSON(http.StatusOK, "ok")
		} else {
			c.JSON(http.StatusUnauthorized, gin.H{"status": "unauthorized"})
		}
	})

	router.GET("/amts", func(c *gin.Context) {
		c.JSON(http.StatusOK, getAttackMonitoringTrees())
	})

	router.POST("/import", func(c *gin.Context) {
		file, _, err := c.Request.FormFile("file"); if err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
			return
		}
		var buf bytes.Buffer
		if _, err := io.Copy(&buf, file); err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
			return
		}
		var attackMonitoringTrees []attackMonitoringTree
		json.Unmarshal(buf.Bytes(), &attackMonitoringTrees); if err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
			return
		}
		err = storeAttackMonitoringTrees(append(getAttackMonitoringTrees(), attackMonitoringTrees...));
		if err != nil {
			log.Error(err)
			c.JSON(http.StatusInternalServerError, "")
		} else {
			c.JSON(http.StatusOK, getAttackMonitoringTrees())
		}
	})

	router.POST("/amts", func(c *gin.Context) {
		//TODO LOG REQUEST/RESPONSE
		var attackMonitoringTrees []attackMonitoringTree
		err := c.BindJSON(&attackMonitoringTrees); if err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
			return
		}
		err = storeAttackMonitoringTrees(attackMonitoringTrees);
		if err != nil {
			log.Error(err)
			c.JSON(http.StatusInternalServerError, "")
		} else {
			c.JSON(http.StatusOK, attackMonitoringTrees)
		}
	})

	router.GET("/specification", func(c *gin.Context) {
		c.JSON(http.StatusOK, getSpecification())
	})

	router.GET("/tools/:name/services", func(c *gin.Context) {
		c.JSON(http.StatusOK, getServicesForTool(monitoring.Tool(c.Param("name"))))
	})

}