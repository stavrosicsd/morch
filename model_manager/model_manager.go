package main

import (
	"bitbucket.org/stavrosicsd/morch/common/logger"
	"bitbucket.org/stavrosicsd/morch/model_manager/amt"
	"bitbucket.org/stavrosicsd/morch/model_manager/config"
	"bitbucket.org/stavrosicsd/morch/model_manager/router"
	"fmt"
	"net/http"
	"strconv"
)

func main() {
	defer logger.Init(config.Conf.Application.Name).Close()
	amt.InitializeEsClient()
	server := config.Conf.Server
	err := http.ListenAndServeTLS(":" + strconv.Itoa(server.Port), server.Cert, server.Key, router.Router()); if err != nil {
		panic(fmt.Errorf("Failed to initialize the Model Manager! Error: %v", err))
	}
}
