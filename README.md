Description
-----

This repository contains the code and example artifacts related to the paper: **Causality-based Accountability Mechanisms for Socio-Technical Systems**
Please cite the paper when using this tool: 

Amjad Ibrahim, Stavros Kyriakopoulos, Alexander Pretschner, Causality-based Accountability Mechanisms for Socio-Technical Systems, Journal of Responsible Technology, Volumes Will appear soon, 2021.


The front-end code is [in this repository](https://bitbucket.org/stavrosicsd/morch-frontend/src/master/).

A list of related tools can be found on [this page (under Threat Modeling and Causality)](https://www.in.tum.de/en/i04/software/)


Setup Production Hosts
-----
zeus: public ip: 138.246.233.54, private ip: 192.168.128.125
hades: public ip: 138.246.233.57, private ip: 192.168.128.37

Setup
-----
0) On all production hosts:
sudo apt-get update &&
sudo apt-get upgrade &&
sudo apt-get dist-upgrade &&
sudo reboot

1) (Optional) setup /etc/hosts, e.g. for hosts, zeus (192.168.128.125), hades(192.168.128.37):
zeus:
192.168.128.37 hades
hades:
192.168.128.125 zeus

2) Open firewall ports necessary for setting up morch and a docker swarm cluster:
zeus:
sudo ufw enable &&
sudo ufw allow 22 &&
sudo ufw allow 9082 &&
sudo ufw allow 5601 &&
sudo ufw allow from 192.168.128.37 to any port 2377 proto tcp &&
sudo ufw allow from 192.168.128.37 to any port 7946 proto tcp &&
sudo ufw allow from 192.168.128.37 to any port 7946 proto udp &&
sudo ufw allow from 192.168.128.37 to any port 4789 proto udp

hades:
sudo ufw enable &&
sudo ufw allow 22 &&
sudo ufw allow 5601 &&
sudo ufw allow from 192.168.128.125 to any port 2377 proto tcp &&
sudo ufw allow from 192.168.128.125 to any port 7946 proto tcp &&
sudo ufw allow from 192.168.128.125 to any port 7946 proto udp &&
sudo ufw allow from 192.168.128.125 to any port 4789 proto udp

2) Setup docker swarm cluster
2.1) Install docker on all hosts
sudo apt-get install apt-transport-https ca-certificates curl gnupg-agent software-properties-common &&
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add - &&
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" &&
sudo apt-get update &&
sudo apt-get install docker docker-compose

2.2) Add your user to the docker group: sudo usermod -a -G docker $USER
2.3) Setup docker swarm cluster
zeus: docker swarm init
hades: copy the output of the previous command on zeus (docker swarm join --token 'token, host and port displayed')

2) Auditd installed on all hosts: sudo apt-get install auditd
3) Create a docker hub account: https://hub.docker.com/

Through this setup we have one manager node (i.e. zeus) and one worker node. We should always have one manager node
because the model manager's frontend and backend as well as kibana need to run together always on the same host (i.e. node),
the manager node.

Morch Setup
-----------
1) Setup ssh for audit containers so they can ssh to the host to restart auditd
1.1) Add the audit container's ssh private key to docker swarm so that any audit container can fetch it.
scp plugins/audit/resources/id_rsa ubuntu@138.246.233.54:/tmp
cat /tmp/id_rsa | docker secret create audit-ssh -
1.2) Add the Morch audit plugin container's ssh public key to the root authorized_keys of all hosts:
scp plugins/audit/resources/id_rsa.pub ubuntu@138.246.233.54:/tmp
scp plugins/audit/resources/id_rsa.pub ubuntu@138.246.233.57:/tmp
zeus, hades: sudo cat /tmp/id_rsa.pub >> /root/.ssh/authorized_keys && sudo rm /tmp/id_rsa.pub
2) Save your docker hub credentials on all cluster hosts so they can pull the images for morch:
zeus, hades: docker login --username=your_docker_hub_username
If you receive the following error message: 'Error saving credentials: error storing credentials - err: exit status 1, out: `Cannot autolaunch D-Bus without X11 $DISPLAY`'
run the following command and retry login: sudo apt install gnupg2 pass
3) Deploy and run morch:
3.1) Replace the DOCKER_HUB_ACCOUNT in the common/common.sh with your account.
3.2) Replace the ssh_user in the morch.sh with the username of your ssh user on the production host.
3.3) Build, deploy and run the system on the cluster: ./morch prod

Note: Because of a known problem (minor design change is necessary) we also need to create the mariadb log file on all hosts:
sudo mkdir /var/log/mysql && sudo touch /var/log/mysql/server_audit.log && sudo chmod 666 /var/log/mysql/server_audit.log

Morch Usage
-----------
1) Disable ipv6 on your local host: add the following lines in the /etc/sysctl.conf:
net.ipv6.conf.default.disable_ipv6=1
net.ipv6.conf.all.disable_ipv6=1
2) Open https://morch.sytes.net and login with your credentials
3) In the workbench import the sample_amts.json (if not already imported)