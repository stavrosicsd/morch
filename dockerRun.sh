#!/bin/bash
for composeFileName in *-docker-compose.yml ; do
    IFS='-' read -ra composeFileNameSplit <<< "$composeFileName"
    docker stack deploy --prune --with-registry-auth -c $composeFileName "${composeFileNameSplit[0]}"
done