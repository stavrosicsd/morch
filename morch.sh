#!/bin/bash

source common/common.sh

ssh_user=ubuntu
staging=/tmp/morch
project_dir=$(pwd)
environment=$1

run_production() {
    ssh_connection=$ssh_user@zeus
    ssh  $ssh_connection "mkdir -p \"$staging\"" &&
    scp -r *-docker-compose.yml plugins/*/*-docker-compose.yml dockerRun.sh $ssh_connection:$staging &&
    ssh $ssh_connection "cd \"$staging\" && export MORCH_ENV=prod && export DOCKER_HUB_ACCOUNT=\"$DOCKER_HUB_ACCOUNT\" && ./dockerRun.sh && cd && rm -rf \"$staging\""
}

run_local() {
    if [ ! -d $staging ]; then
        mkdir $staging
    fi &&
    cp -r *-docker-compose.yml $staging &&
    cp -r plugins/*/*-docker-compose.yml $staging &&
    cp dockerRun.sh $staging
    cd $staging &&
    ./dockerRun.sh &&
    cd $project_dir &&
    rm -rf $staging
}

run() {
    if [[ $environment = "prod" ]]; then
       run_production
    else
      run_local
    fi
}

build_subprojects &&
run